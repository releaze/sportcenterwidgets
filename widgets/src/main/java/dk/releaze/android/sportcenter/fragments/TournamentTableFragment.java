package dk.releaze.android.sportcenter.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.fragments.controllers.TournamentTableViewController;
import dk.releaze.android.sportcenter.fragments.loaders.LeagueTypedLeagueTableItemsLoader;
import dk.releaze.android.sportcenter.model.vo.TypedLeagueTableItems;
import dk.releaze.android.sportcenter.utils.async.SimpleLoaderCallbacks;

/**
 * Created by gpl on 24.10.14.
 */
public class TournamentTableFragment extends Fragment {
    public static final String TAG = TournamentTableFragment.class.getSimpleName();
    private static final String EXTRA_TOURNAMENT_ID = TAG + ".EXTRA_TOURNAMENT_ID";

    public static TournamentTableFragment newInstance(int tournamentId) {
        TournamentTableFragment fragment = new TournamentTableFragment();
        fragment.setArguments(withArgs(tournamentId));
        return fragment;
    }

    public static Bundle withArgs(int tournamentId) {
        Bundle arguments = new Bundle();
        arguments.putInt(EXTRA_TOURNAMENT_ID, tournamentId);
        return arguments;
    }

    private int mTournamentId;
    private String mNetworkErrorMessage;
    private boolean mIsNetworkErrorMessageEnabled;
    private String mCorruptedDataMessage;
    private boolean mIsCorruptedDataMessageEnabled;
    private TournamentTableViewController mTournamentTableViewController = new TournamentTableViewController();

    private LoaderManager.LoaderCallbacks<TypedLeagueTableItems> mCallbacks = new SimpleLoaderCallbacks<TypedLeagueTableItems>() {
        @Override
        public Loader<TypedLeagueTableItems> onCreateLoader(int i, Bundle bundle) {
            return new LeagueTypedLeagueTableItemsLoader(getActivity(), mTournamentId);
        }

        @Override
        public void onLoadFinished(Loader<TypedLeagueTableItems> typedLeagueTableItemsLoader,
                                   TypedLeagueTableItems typedLeagueTableItems) {
            if (typedLeagueTableItems == null) {
                Log.e(TAG, "Connection error");
                if (mIsNetworkErrorMessageEnabled &&
                        !TextUtils.isEmpty(mNetworkErrorMessage) &&
                        isAdded()) {
                    Toast.makeText(getActivity(), mNetworkErrorMessage, Toast.LENGTH_SHORT).show();
                }
                return;
            }
            if (typedLeagueTableItems.getAway() == null ||
                    typedLeagueTableItems.getHome() == null ||
                    typedLeagueTableItems.getReal() == null) {
                Log.e(TAG, "Corrupted data");
                if (mIsCorruptedDataMessageEnabled &&
                        !TextUtils.isEmpty(mCorruptedDataMessage) &&
                        isAdded()) {
                    Toast.makeText(getActivity(), mCorruptedDataMessage, Toast.LENGTH_SHORT).show();
                }
            }
            // TODO update
            mTournamentTableViewController.update(typedLeagueTableItems);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null) throw new IllegalStateException("Arguments shouldn't be null");
        if (!arguments.containsKey(EXTRA_TOURNAMENT_ID))
            throw new IllegalStateException("Invalid arguments");
        mTournamentId = getArguments().getInt(EXTRA_TOURNAMENT_ID);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, mCallbacks);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dk_releaze_view_league_table, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTournamentTableViewController.setView(view);
        mTournamentTableViewController.setTitle(getString(R.string.dk_releaze_tournament_table_title));
    }

    public void setNetworkErrorMessage(String message) {
        mNetworkErrorMessage = message;
    }

    public void setCorruptedDataMessage(String message) {
        mCorruptedDataMessage = message;
    }

    public void setCorruptedDataMessageEnabled(boolean isEnabled) {
        mIsCorruptedDataMessageEnabled = isEnabled;
    }

    public void setNetworkErrorMessageEnabled(boolean isEnabled) {
        mIsNetworkErrorMessageEnabled = isEnabled;
    }
}
