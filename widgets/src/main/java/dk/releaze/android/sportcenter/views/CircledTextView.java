package dk.releaze.android.sportcenter.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by gpl on 24.10.14.
 */
public class CircledTextView extends TextView {
    public static final int DEFAULT_CIRCLE_COLOR = Color.TRANSPARENT;
    private int mCircleColor = DEFAULT_CIRCLE_COLOR;

    public CircledTextView(Context context) {
        super(context);
    }

    public CircledTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircledTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawCircle(canvas);
        super.onDraw(canvas);
    }

    public void drawCircle(int circleColor) {
        mCircleColor = circleColor;
        invalidate();
    }

    public void unDrawCircle() {
        mCircleColor = DEFAULT_CIRCLE_COLOR;
        invalidate();
    }

    private void drawCircle(Canvas canvas) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mCircleColor);
        float centerX = getWidth() / 2;
        float centerY = getHeight() / 2;
        float radius = (getWidth() + getHeight()) / 4;
        canvas.drawCircle(centerX, centerY, radius, paint);
    }
}
