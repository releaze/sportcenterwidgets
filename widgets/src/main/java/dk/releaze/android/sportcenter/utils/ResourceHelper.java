package dk.releaze.android.sportcenter.utils;

import android.content.Context;

/**
 * Created by gpl on 24.10.14.
 */
public class ResourceHelper {
    public static final int RESOURCE_UNDEFINED = 0x0;

    public enum ResourceType {
        DRAWABLE("drawable"), STRING("string"), INTEGER("integer"), COLOR("color");
        String name;

        ResourceType(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }
    }

    private ResourceHelper() {/* to prevent instantiation */}

    public static boolean isNotFound(int resId) {
        return resId == RESOURCE_UNDEFINED;
    }

    public static int getDrawableId(String name, Context context) {
        return getResourceId(name, context, ResourceType.DRAWABLE);
    }

    public static int getColorId(String name, Context context) {
        return getResourceId(name, context, ResourceType.COLOR);
    }

    public static int getStringId(String name, Context context) {
        return getResourceId(name, context, ResourceType.STRING);
    }

    public static int getIntegerId(String name, Context context) {
        return getResourceId(name, context, ResourceType.INTEGER);
    }

    public static int getResourceId(String name, Context context, ResourceType type) {
        return context.getResources().getIdentifier(name, type.getName(), context.getPackageName());
    }
}
