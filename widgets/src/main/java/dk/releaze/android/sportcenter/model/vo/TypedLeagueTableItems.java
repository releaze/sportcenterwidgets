package dk.releaze.android.sportcenter.model.vo;

import java.util.Collections;
import java.util.List;



/**
 * Created by gpl on 24.10.14.
 */
public class TypedLeagueTableItems {
    private List<LeagueTableItem> real;
    private List<LeagueTableItem> home;
    private List<LeagueTableItem> away;

    public List<LeagueTableItem> getReal() {
        return Collections.unmodifiableList(real);
    }

    public List<LeagueTableItem> getHome() {
        return Collections.unmodifiableList(home);
    }

    public List<LeagueTableItem> getAway() {
        return Collections.unmodifiableList(away);
    }

}
