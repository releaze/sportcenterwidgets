package dk.releaze.android.sportcenter.fragments.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import dk.releaze.android.sportcenter.io.Api;
import dk.releaze.android.sportcenter.io.RetrofitException;
import dk.releaze.android.sportcenter.model.vo.TypedLeagueTableItems;
import dk.releaze.android.sportcenter.utils.ApiInitializer;

/**
 * Created by gpl on 24.10.14.
 */
public class LeagueTypedLeagueTableItemsLoader extends AsyncTaskLoader<TypedLeagueTableItems> {
    private final int mTournamentId;

    public LeagueTypedLeagueTableItemsLoader(Context context, int tournamentId) {
        super(context);
        mTournamentId = tournamentId;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public TypedLeagueTableItems loadInBackground() {
        try {
            return ApiInitializer.getInstance(getContext()).get().leagueTableByType(mTournamentId, Api.SPORT_ALL);
        } catch (RetrofitException e) {
            return null;
        }
    }
}
