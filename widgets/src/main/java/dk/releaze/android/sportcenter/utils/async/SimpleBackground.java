package dk.releaze.android.sportcenter.utils.async;

import android.os.AsyncTask;

/**
 * Created by gpl on 29.10.14.
 */
public class SimpleBackground<T, V> {
    static <T, V> SimpleBackground<T, V> with(T input) {
        return new SimpleBackground<T, V>(input);
    }

    public static <V> SimpleBackground<Object, V> empty() {
        return new SimpleBackground<Object, V>(new Object());
    }

    public SimpleBackground<T, V> work(Work<T, V> work) {
        mWork = work;
        return this;
    }

    public SimpleBackground<T, V> result(Result<V> result) {
        mResult = result;
        return this;
    }

    public void execute() {
        new Async<T, V>(mWork, mResult).execute(mInput);
    }

    public static interface Work<T, V> {
        V proceed(T t);
    }

    public static interface Result<V> {
        void proceed(V v);
    }

    private final T mInput;
    private Work<T, V> mWork;
    private Result<V> mResult;

    private SimpleBackground(T input) {
        mInput = input;
    }

    private static final class Async<T, V> extends AsyncTask<T, Void, V> {

        private Work<T, V> mWork;
        private Result<V> mResult;

        Async(Work<T, V> work, Result<V> result) {
            mWork = work;
            mResult = result;
        }

        @Override
        protected V doInBackground(T... ts) {
            if (ts == null || ts.length == 0) return null;
            return mWork.proceed(ts[0]);
        }

        @Override
        protected void onPostExecute(V v) {
            mResult.proceed(v);
        }
    }
}
