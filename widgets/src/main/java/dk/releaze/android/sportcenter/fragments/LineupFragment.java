package dk.releaze.android.sportcenter.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.fragments.controllers.SoccerFieldViewController;
import dk.releaze.android.sportcenter.fragments.loaders.LineUpLoader;
import dk.releaze.android.sportcenter.model.Event;
import dk.releaze.android.sportcenter.model.LineUpItem;
import dk.releaze.android.sportcenter.utils.ImageResourceHelper;
import dk.releaze.android.sportcenter.utils.async.SimpleBackground;
import dk.releaze.android.sportcenter.utils.async.SimpleLoaderCallbacks;
import dk.releaze.android.sportcenter.views.QuickActionGrid;


/**
 * Created by gpl on 24.10.14.
 */
public class LineupFragment extends Fragment {
    public static final String TAG = LineupFragment.class.getSimpleName();
    private static final String EXTRA_EVENT_ID = TAG + ".EXTRA_EVENT_ID";

    public static LineupFragment newInstance(int eventId) {
        LineupFragment fragment = new LineupFragment();
        fragment.setArguments(withArgs(eventId));
        return fragment;
    }

    public static Bundle withArgs(int eventId) {
        Bundle arguments = new Bundle();
        arguments.putInt(EXTRA_EVENT_ID, eventId);
        return arguments;
    }

    private List<LineUpItem> mHomeTeam;

    private List<LineUpItem> mAwayTeam;
    private SoccerFieldViewController mField = new SoccerFieldViewController();
    private ToggleButton tbHome, tbAway;
    private QABenchAdapter adapterHomeBench, adapterAwayBench;
    private QuickActionGrid qaHomeBench, qaAwayBench;
    private int mEventId;

    protected Event mEvent;

    private LoaderManager.LoaderCallbacks<LineUpLoader.Result> mCallbacks =
            new SimpleLoaderCallbacks<LineUpLoader.Result>() {
                @Override
                public Loader<LineUpLoader.Result> onCreateLoader(int i, Bundle bundle) {
                    return new LineUpLoader(getActivity(), mEventId);
                }

                @Override
                public void onLoadFinished(Loader<LineUpLoader.Result> mapLoader, LineUpLoader.Result data) {
                    if (data != null) {
                        mEvent = data.getEvent();
                        mHomeTeam = data.getHomeTeam();
                        mAwayTeam = data.getAwayTeam();
                        if (mHomeTeam != null && mAwayTeam != null) {
                            mField.initData(mHomeTeam, mAwayTeam, mEvent);
                            initHomeBench(mHomeTeam);
                            initAwayBench(mAwayTeam);
                        }
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null) throw new IllegalStateException("Arguments shouldn't be null");
        if (!arguments.containsKey(EXTRA_EVENT_ID))
            throw new IllegalStateException("Invalid arguments");
        mEventId = arguments.getInt(EXTRA_EVENT_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dk_releaze_view_lineup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mField.setView(view.findViewById(R.id.ed_lineup__field));
        tbHome = (ToggleButton) view.findViewById(R.id.ed_lineup__home_bench_button);
        tbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (qaHomeBench != null) qaHomeBench.show(view);
            }
        });
        tbAway = (ToggleButton) view.findViewById(R.id.ed_lineup__away_bench_button);
        tbAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (qaHomeBench != null) qaAwayBench.show(view);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, mCallbacks);
    }

    private void initHomeBench(final List<LineUpItem> players) {
        SimpleBackground<Object, List<LineUpItem>> sb = SimpleBackground.empty();
        sb.work(new SimpleBackground.Work<Object, List<LineUpItem>>() {
            @Override
            public List<LineUpItem> proceed(Object v) {
                return getSubs(players);
            }
        }).result(new SimpleBackground.Result<List<LineUpItem>>() {
            @Override
            public void proceed(List<LineUpItem> subs) {
                setHomeBench(mEvent, subs);
            }
        }).execute();
    }

    private void initAwayBench(final List<LineUpItem> players) {
        SimpleBackground<Object, List<LineUpItem>> sb = SimpleBackground.empty();
        sb.work(new SimpleBackground.Work<Object, List<LineUpItem>>() {
            @Override
            public List<LineUpItem> proceed(Object o) {
                return getSubs(players);
            }
        }).result(new SimpleBackground.Result<List<LineUpItem>>() {
            @Override
            public void proceed(List<LineUpItem> subs) {
                setAwayBench(mEvent, subs);
            }
        }).execute();
    }

    private List<LineUpItem> getSubs(List<LineUpItem> players) {
        List<LineUpItem> items = new ArrayList<LineUpItem>();
        for (LineUpItem item : players) {
            if (item.isSubs()) items.add(item);
        }
        return items;
    }

    private void setHomeBench(Event event, List<LineUpItem> homeTeam) {
        if (!isAdded()) return;
        qaHomeBench = new QuickActionGrid(getActivity());
        qaHomeBench.setNumColumns(2);
        qaHomeBench.setRootWidth(tbHome.getMeasuredWidth());
        qaHomeBench.setAdapter(adapterHomeBench = new QABenchAdapter(getActivity(), event.getHomeId(), true));
        adapterHomeBench.addAll(homeTeam);
        qaHomeBench.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                tbHome.setChecked(false);
            }
        });
    }

    private void setAwayBench(Event event, List<LineUpItem> awayTeam) {
        if (!isAdded()) return;
        qaAwayBench = new QuickActionGrid(getActivity());
        qaAwayBench.setNumColumns(2);
        qaAwayBench.setRootWidth(tbAway.getMeasuredWidth());
        qaAwayBench.setAdapter(adapterAwayBench = new QABenchAdapter(getActivity(), event.getAwayId(), false));
        adapterAwayBench.addAll(awayTeam);
        qaAwayBench.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                tbAway.setChecked(false);
            }
        });
    }


    public static class QABenchAdapter extends ArrayAdapter<LineUpItem> {
        private boolean home = false;
        private int teamId;
        private int w, h;

        public QABenchAdapter(Context context, int teamId, boolean home) {
            super(context, R.layout.dk_releaze_view_lineup_bench_player);
            this.w = context.getResources().getDimensionPixelSize(R.dimen.dk_releaze_live_list_tshirt_width);
            this.h = context.getResources().getDimensionPixelSize(R.dimen.dk_releaze_live_list_tshirt_height);
            this.home = home;
            this.teamId = teamId;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dk_releaze_view_lineup_bench_player, null);

                viewHolder = new ViewHolder();
                viewHolder.name = (TextView) convertView.findViewById(R.id.ed__lineup_bench_player_name);
                viewHolder.shirt = (ImageView) convertView.findViewById(R.id.ed__lineup_bench_player_shirt);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final LineUpItem item = getItem(position);

            viewHolder.name.setText(item.getName());

            String tshirtUrl = ImageResourceHelper.getInstance(getContext()).getTShirtUrl(teamId, w, h, home);
            ImageLoader.getInstance().displayImage(tshirtUrl, viewHolder.shirt);

            convertView.setId(position);
            return convertView;
        }

        static class ViewHolder {
            TextView name;
            ImageView shirt;
        }
    }
}