package dk.releaze.android.sportcenter.utils.async;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

/**
 * Created by gpl on 24.10.14.
 */
public class SimpleLoaderCallbacks<D> implements LoaderManager.LoaderCallbacks<D> {
    @Override
    public Loader<D> onCreateLoader(int i, Bundle bundle) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onLoadFinished(Loader<D> dLoader, D d) {/* empty */}

    @Override
    public void onLoaderReset(Loader<D> dLoader) {/* empty */}
}
