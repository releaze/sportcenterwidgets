
package dk.releaze.android.sportcenter.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by gpl on 24.10.14.
 */
public class Event {
    private int eventId;
    private int homeId;
    private int awayId;
    private int countryId;
    private int tournamentStageId;
    private int tournamentId;
    private int tournamentTemplateId;
    private int sportId;
    private int version;
    private Date startDate;
    private String statusType;
    private String statusFull;
    private String homeName;
    private String awayName;
    private String homeName2;
    private String awayName2;

    private String sHomeName;
    private String sAwayName;
    private String country;
    private String tournamentName;
    private String tournamentYear;

    private ArrayList<Incident> incidents = new ArrayList<Incident>();
    public int getHomeId() {
        return homeId;
    }
    public void setHomeId(int homeId) {
        this.homeId = homeId;
    }
    public int getAwayId() {
        return awayId;
    }
    public void setAwayId(int awayId) {
        this.awayId = awayId;
    }
    public List<Incident> getIncidents() {
        return incidents;
    }
    public boolean isLive() {
        return statusType.equals("inprogress");
    }
    public boolean isComming() {
        return statusType.equals("notstarted");
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getTournamentStageId() {
        return tournamentStageId;
    }

    public void setTournamentStageId(int tournamentStageId) {
        this.tournamentStageId = tournamentStageId;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getTournamentTemplateId() {
        return tournamentTemplateId;
    }

    public void setTournamentTemplateId(int tournamentTemplateId) {
        this.tournamentTemplateId = tournamentTemplateId;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getStatusFull() {
        return statusFull;
    }

    public void setStatusFull(String statusFull) {
        this.statusFull = statusFull;
    }

    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public String getAwayName() {
        return awayName;
    }

    public void setAwayName(String awayName) {
        this.awayName = awayName;
    }

    public String getHomeName2() {
        return homeName2;
    }

    public void setHomeName2(String homeName2) {
        this.homeName2 = homeName2;
    }

    public String getAwayName2() {
        return awayName2;
    }

    public void setAwayName2(String awayName2) {
        this.awayName2 = awayName2;
    }

    public String getsHomeName() {
        return sHomeName;
    }

    public void setsHomeName(String sHomeName) {
        this.sHomeName = sHomeName;
    }

    public String getsAwayName() {
        return sAwayName;
    }

    public void setsAwayName(String sAwayName) {
        this.sAwayName = sAwayName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getTournamentYear() {
        return tournamentYear;
    }

    public void setTournamentYear(String tournamentYear) {
        this.tournamentYear = tournamentYear;
    }

    public void setIncidents(ArrayList<Incident> incidents) {
        this.incidents = incidents;
    }
}

