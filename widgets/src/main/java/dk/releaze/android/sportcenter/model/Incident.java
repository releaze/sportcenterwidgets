
package dk.releaze.android.sportcenter.model;

/**
 * Created by gpl on 24.10.14.
 */
public class Incident {
    private int incidentId;
    private int min;
    private int typeId;
    private String type;
    private String subtype;
    private String teamName;
    private int teamId;
    private int participantId;
    private String participantName;
    private boolean home;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getIncidentId() {
        return incidentId;
    }

    public int getMin() {
        return min;
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getTeamId() {
        return teamId;
    }

    public int getParticipantId() {
        return participantId;
    }

    public String getParticipantName() {
        return participantName;
    }

    public boolean isHome() {
        return home;
    }

    public boolean isGoalScored() {
        return subtype.equals("goal") && typeId != 9 && typeId != 11 && typeId != 12 && typeId != 19;
    }
}
