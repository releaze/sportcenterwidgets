package dk.releaze.android.sportcenter.views;

import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by gpl on 24.10.14.
 */
public class ToggleButtonSelector {
    private List<ToggleButton> mButtons = new ArrayList<ToggleButton>();

    public void add(ToggleButton button) {
        mButtons.add(button);
    }

    public void select(ToggleButton button) {
        for (ToggleButton b : mButtons) {
            if (b.equals(button)) {
                b.setChecked(true);
            } else {
                b.setChecked(false);
            }
        }
    }

    public void remove(ToggleButton button) {
        mButtons.remove(button);
    }

    public void removeAll() {
        mButtons.clear();
    }

    public List<ToggleButton> getToggleButtons() {
        return Collections.unmodifiableList(mButtons);
    }
}
