package dk.releaze.android.sportcenter.fragments.controllers;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.model.Event;
import dk.releaze.android.sportcenter.model.LineUpItem;
import dk.releaze.android.sportcenter.utils.ImageLoaderInitializer;
import dk.releaze.android.sportcenter.utils.ImageResourceHelper;
import dk.releaze.android.sportcenter.utils.async.SimpleBackground;


/**
 * Created by gpl on 24.10.14.
 */
public class SoccerFieldViewController {
    private Event event;
    private RelativeLayout rlHomeContainer, rlAwayContainer;
    private List<LineUpPlayerViewController> homeItems = new ArrayList<LineUpPlayerViewController>();
    private List<LineUpPlayerViewController> awayItems = new ArrayList<LineUpPlayerViewController>();
    private View mView;
    private int mPlayerProfileWidth;

    public void setView(View view) {
        mView = view;
        rlHomeContainer = (RelativeLayout) view.findViewById(R.id.ed_lineup__home);
        rlAwayContainer = (RelativeLayout) view.findViewById(R.id.ed_lineup__away);
        mPlayerProfileWidth = mView.getResources().getDimensionPixelSize(R.dimen.dk_releaze_lineup_player_width);
    }

    public SoccerFieldViewController initData(List<LineUpItem> homeTeam, List<LineUpItem> awayTeam, Event event) {
        this.event = event;
        TeamInfoHolder homeHolder = new TeamInfoHolder(homeTeam, event.getHomeId(), R.id.ed_lineup__home, true);
        homeHolder.coachField = R.id.ed_lineup__home_coach;
        homeHolder.tacticField = R.id.ed_lineup__home_tactic;
        TeamInfoHolder awayHolder = new TeamInfoHolder(awayTeam, event.getAwayId(), R.id.ed_lineup__away, false);
        awayHolder.coachField = R.id.ed_lineup__away_coach;
        awayHolder.tacticField = R.id.ed_lineup__away_tactic;
        processTeam(rlHomeContainer, homeHolder, homeItems);
        processTeam(rlAwayContainer, awayHolder, awayItems);
        return this;
    }

    private void processTeam(final RelativeLayout container, final TeamInfoHolder teamInfo, final List<LineUpPlayerViewController> playerViews) {
        if (!playerViews.isEmpty()) for (LineUpPlayerViewController controller : playerViews)
            container.removeView(controller.getView());
        SimpleBackground<Object, List<LineUpPlayerViewController>> sb = SimpleBackground.empty();
        sb.work(new SimpleBackground.Work<Object, List<LineUpPlayerViewController>>() {
            @Override
            public List<LineUpPlayerViewController> proceed(Object o) {
                List<LineUpItem> startingPlayers = new ArrayList<LineUpItem>();
                for (LineUpItem item : teamInfo.players) {
                    if (item.getPositionCode() != 0) startingPlayers.add(item);
                }
                Map<Integer, List<LineUpItem>> indexedFormation = new HashMap<Integer, List<LineUpItem>>();
                for (LineUpItem item : startingPlayers) {
                    Integer posY = item.getPositionY();
                    if (!indexedFormation.containsKey(posY))
                        indexedFormation.put(posY, new ArrayList<LineUpItem>());
                    indexedFormation.get(posY).add(item);
                }
                calculatePlayerPositions(indexedFormation, teamInfo.homeTeam);

                if (startingPlayers.size() == 11) {
                    playerViews.clear();
                    for (int i = 0; i < 11; i++) {
                        LineUpItem player = startingPlayers.get(i);
                        LineUpPlayerViewController view = new LineUpPlayerViewController();
                        View lineupPlayerLayout = LayoutInflater.from(mView.getContext())
                                .inflate(R.layout.dk_releaze_view_lineup_player, (ViewGroup) mView, false);
                        view.setView(lineupPlayerLayout);
                        view.updateWith(player, event);
                        playerViews.add(view);
                    }
                }
                return playerViews;
            }
        }).result(new SimpleBackground.Result<List<LineUpPlayerViewController>>() {
            @Override
            public void proceed(List<LineUpPlayerViewController> lineUpPlayerViewControllers) {
                container.removeAllViews();
                for (LineUpPlayerViewController controller : playerViews)
                    container.addView(controller.getView());
                loadTShirts(teamInfo, playerViews);
            }
        }).execute();

        /* Coach name */
        SimpleBackground<Object, LineUpItem> sb1 = SimpleBackground.empty();
        sb1.work(new SimpleBackground.Work<Object, LineUpItem>() {
            @Override
            public LineUpItem proceed(Object o) {
                LineUpItem result = null;
                for (LineUpItem item : teamInfo.players) {
                    if (item.getPositionEnetType() == 10) {
                        result = item;
                        break;
                    }
                }
                return result;
            }
        }).result(new SimpleBackground.Result<LineUpItem>() {
            @Override
            public void proceed(LineUpItem coach) {
                if (coach == null) return;
                TextView view = (TextView) mView.findViewById(teamInfo.coachField);
                if (view != null) view.setText(coach.getName());
            }
        }).execute();

        /* Team tactic */
        SimpleBackground<Object, String> sb2 = SimpleBackground.empty();
        sb2.work(new SimpleBackground.Work<Object, String>() {
            @Override
            public String proceed(Object o) {
                List<LineUpItem> startingPlayers = new ArrayList<LineUpItem>();
                for (LineUpItem item : teamInfo.players) {
                    if (item.getPositionCode() != 0) startingPlayers.add(item);
                }
                Map<Integer, List<LineUpItem>> indexedFormation = new HashMap<Integer, List<LineUpItem>>();
                for (LineUpItem item : startingPlayers) {
                    Integer posY = item.getPositionY();
                    if (!indexedFormation.containsKey(posY))
                        indexedFormation.put(posY, new ArrayList<LineUpItem>());
                    indexedFormation.get(posY).add(item);
                }
                return initTactic(indexedFormation);
            }
        }).result(new SimpleBackground.Result<String>() {
            @Override
            public void proceed(String tactic) {
                TextView view = (TextView) mView.findViewById(teamInfo.tacticField);
                if (view != null) view.setText(tactic);
            }
        }).execute();
    }

    private String initTactic(Map<Integer, List<LineUpItem>> yMap /*Multimap<Integer, LineUpItem> yMap*/) {
        Integer[] arr = new Integer[yMap.keySet().size()];
        yMap.keySet().toArray(arr);
        List<Integer> sortedArray = Arrays.asList(arr);
        Collections.sort(sortedArray);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < sortedArray.size(); i++) { // start from 1 for skip goalkeeper
            if (stringBuilder.length() != 0) {
                stringBuilder.append("-");
            }
            int key = sortedArray.get(i);
            stringBuilder.append(yMap.get(Integer.valueOf(key)).size());
        }
        return stringBuilder.toString();
    }

    private void calculatePlayerPositions(Map<Integer, List<LineUpItem>> map /*ArrayListMultimap<Integer, LineUpItem> map*/, final boolean home) {
        List<Integer> horizontalLines = new ArrayList<Integer>(map.keySet());
        Collections.sort(horizontalLines);

        for (Integer row : horizontalLines) {
            List<LineUpItem> lineupRow = map.get(row);
            Collections.sort(lineupRow, new Comparator<LineUpItem>() {
                @Override
                public int compare(LineUpItem lhs, LineUpItem rhs) {
                    if (home) {
                        if (lhs.getPositionX() < rhs.getPositionX()) return -1;
                        if (lhs.getPositionX() > rhs.getPositionX()) return 1;
                    } else {
                        if (lhs.getPositionX() < rhs.getPositionX()) return 1;
                        if (lhs.getPositionX() > rhs.getPositionX()) return -1;
                    }
                    return 0;

                }
            });
            float cellX = (float) mView.getMeasuredWidth() / (lineupRow.size() + 1); // 10 - possible player positions
            float cellY = (float) (mView.getMeasuredHeight() / 2 - 10) / horizontalLines.size();
            for (final LineUpItem player : lineupRow) {
                float x = 0.0f, y = 0.0f;
                int playerIndex = lineupRow.indexOf(player);
                int playerYRow = horizontalLines.indexOf(player.getPositionY());
                x = cellX * (playerIndex + 1) - mPlayerProfileWidth / 2;
                if (horizontalLines.size() > 5 && playerYRow > 3) cellY -= 10;
                if (home) {
                    y = cellY * playerYRow + 10;
                } else {
                    y = Math.abs(mView.getMeasuredHeight() / 2 - (cellY * (playerYRow + 1))) - 10;
                }

                player.setX(x);
                player.setY(y);
            }

        }
    }

    private class TeamInfoHolder {
        int teamId;
        final private List<LineUpItem> players;
        final int fieldPartLayout;

        boolean homeTeam;
        int tacticField;
        int coachField;

        private TeamInfoHolder(List<LineUpItem> list, int teamId, int fieldPartLayout, boolean homeTeam) {
            this.players = list;
            this.teamId = teamId;
            this.fieldPartLayout = fieldPartLayout;
            this.homeTeam = homeTeam;
        }
    }

    public void loadTShirts(final TeamInfoHolder ti, final List<LineUpPlayerViewController> lineup) {
        int w = mView.getResources().getDimensionPixelSize(R.dimen.dk_releaze_lineup_tshirt_height);
        int h = mView.getResources().getDimensionPixelSize(R.dimen.dk_releaze_lineup_tshirt_height);
        String tshirtUrl = ImageResourceHelper.getInstance(mView.getContext()).getTShirtUrl(ti.teamId, w, h, ti.homeTeam);
        ImageLoaderInitializer.getInstance(mView.getContext()).get().loadImage(tshirtUrl, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                for (LineUpPlayerViewController v : lineup) {
                    v.tshirt.setImageResource(ti.homeTeam ? R.drawable.shirt_home_def : R.drawable.shirt_away_def);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                for (LineUpPlayerViewController v : lineup) {
                    v.tshirt.setImageBitmap(loadedImage);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }
}