
package dk.releaze.android.sportcenter.model.vo;

/**
 * Created by gpl on 24.10.14.
 */
public class LeagueTableItem {
    private int teamId;
    private String teamName;
    private int rank;
    private int points;
    private int goalsScored;
    private int goalsConceded;
    private int matchesPlayed;
    private int matchesWon;
    private int matchesLost;
    private int matchesDraw;
    private int standingId;
    private String rankType;

    public int getTeamId() {
        return teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getRank() {
        return rank;
    }

    public int getPoints() {
        return points;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public int getGoalsConceded() {
        return goalsConceded;
    }

    public int getMatchesPlayed() {
        return matchesPlayed;
    }

    public int getMatchesWon() {
        return matchesWon;
    }

    public int getMatchesLost() {
        return matchesLost;
    }

    public int getMatchesDraw() {
        return matchesDraw;
    }

    public String getRankType() {
        return rankType;
    }

    public int getStandingId() {
        return standingId;
    }
}