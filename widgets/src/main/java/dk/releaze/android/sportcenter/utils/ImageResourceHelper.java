package dk.releaze.android.sportcenter.utils;

import android.content.Context;
import android.widget.ImageView;

import java.net.URLEncoder;

import dk.releaze.android.sportcenter.model.Incident;

/**
 * Created by gpl on 24.10.14.
 */
public class ImageResourceHelper {
    private static final String URL_TEAMS_SHIRTS = "http://rips.releaze.dk/rips/rips.ngx/?url=http%3A%2F%2Fsrc.releaze.dk%2Fpublic%2Fsoccer-app%2Fassets%2Fic_shirts_${teamId}.png&amp;width-px=${width}&amp;height-px=${height}&amp;defurl=${defurl}";
    private static final String URL_HOME_SHIRT_DEF = "http://src.releaze.dk/public/soccer-app/assets/shirt_home_def.png";
    private static final String URL_AWAY_SHIRT_DEF = "http://src.releaze.dk/public/soccer-app/assets/shirt_away_def.png";
    private static ImageResourceHelper instance;

    Context context;

    public static ImageResourceHelper getInstance(Context context) {
        if (instance == null) instance = new ImageResourceHelper(context);
        return instance;
    }

    private ImageResourceHelper(Context context) {
        this.context = context;
    }

    int getIncidentImage(Incident incident) {
        // TODO cache incidents resource in memory
        int drawableResourceId = context.getResources().getIdentifier("incident_type_" + incident.getTypeId(), "drawable", context.getPackageName());
        if (drawableResourceId != 0) {
            return drawableResourceId;
        } else {
            return android.R.color.transparent;
        }
    }

    public void bind(Incident incident, ImageView imageView) {
        imageView.setImageResource(getIncidentImage(incident));
    }

    public String getTShirtUrl(int teamId, int w, int h, boolean home) {
        String url = URL_TEAMS_SHIRTS.replace("${teamId}", teamId + "").replace("${width}", w + "").replace("${height}", h + "");
        String def = home ? URL_HOME_SHIRT_DEF : URL_AWAY_SHIRT_DEF;
        url = url.replace("${defurl}", URLEncoder.encode(def));
        return url;
    }
}
