package dk.releaze.android.sportcenter.io;

import java.util.List;

import dk.releaze.android.sportcenter.model.Event;
import dk.releaze.android.sportcenter.model.LineUpItem;
import dk.releaze.android.sportcenter.model.vo.TypedLeagueTableItems;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by gpl on 24.10.14.
 */
public interface Api {

    public static final String SPORT_ALL = "all";
    public static final String SPORT_SOCCER = "soccer";
    public static final String SPORT_BASKET = "basket";

    /**
     * @param tournament
     * @param type
     * @return
     */
    @GET("/tournaments/{tournament}/standings")
    TypedLeagueTableItems leagueTableByType(@Path("tournament") int tournament, @Query("type") String type) throws RetrofitException;

    /**
     *
     * @param eventId
     * @param teamId
     * @return
     */
    @GET("/lineups/enet/{eventId}/{teamId}")
    List<LineUpItem> lineup(@Path("eventId")int eventId, @Path("teamId")int teamId) throws RetrofitException;

    /**
     *
     * @param eventId
     * @return
     */
    @GET("/events/{eventId}")
    Event event(@Path("eventId") long eventId) throws RetrofitException;
}
