package dk.releaze.android.sportcenter.fragments.controllers;

import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dk.releaze.android.sportcenter.BuildConfig;
import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.model.Event;
import dk.releaze.android.sportcenter.model.Incident;
import dk.releaze.android.sportcenter.model.LineUpItem;
import dk.releaze.android.sportcenter.utils.ImageResourceHelper;

/**
 * Created by gpl on 24.10.14.
 */
public class LineUpPlayerViewController {
    private LineUpItem lineupItem;
    private View mView;
    ImageView tshirt;

    public void setView(View view) {
        mView = view;
        tshirt = (ImageView) mView.findViewById(R.id.ed_lineup__player__shirt);
    }

    public View getView() {
        return mView;
    }

    public LineUpPlayerViewController updateWith(LineUpItem lineUpItem, final Event event) {
        lineupItem = lineUpItem;
        ((TextView) mView.findViewById(R.id.ed_lineup__player__name)).setText(lineUpItem.getShortName());
        if (lineUpItem.isNew() && (event.isLive() || event.isComming())) {
            mView.findViewById(R.id.ed_lineup__player__new).setVisibility(View.VISIBLE);
        }
        if (lineUpItem.getNumber() != 0) {
            ((TextView) mView.findViewById(R.id.ed_lineup__player__num)).setText(String.valueOf(lineUpItem.getNumber()));
        }
        addIncidentViews(event);

        if (BuildConfig.VERSION_CODE < 11) {

        }

//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mView.getLayoutParams();
//        params.leftMargin = (int)lineUpItem.getX(); //Your X coordinate
//        params.topMargin = (int)lineUpItem.getY(); //Your Y coordinate
//        mView.setLayoutParams(params);

        mView.setX(lineUpItem.getX());
        mView.setY(lineUpItem.getY());
        return this;
    }

    private void addIncidentViews(Event event) {
        List<Incident> goals = new ArrayList<Incident>();
        for (Incident incident : event.getIncidents()) {
            if (incident.isGoalScored() &&
                    incident.getParticipantId() == lineupItem.getPlayerId()) goals.add(incident);
        }
        /*FluentIterable.from(event.getIncidents()).filter(new Predicate<Incident>() {
            @Override
            public boolean apply(Incident input) {
                return input.isGoalScored() && input.getParticipantId() == lineupItem.getPlayerId();
            }
        }).toList();*/
        TextView goalsLabel = (TextView) mView.findViewById(R.id.goals_label);
        Drawable img = mView.getResources().getDrawable(R.drawable.ic_incident_goal);
        float imgWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, mView.getResources().getDisplayMetrics());
        img.setBounds(0, 0, (int) imgWidth, (int) imgWidth);
        goalsLabel.setCompoundDrawables(img, null, null, null);
        if (goals.size() == 0) {
            goalsLabel.setVisibility(View.GONE);
        } else if (goals.size() == 1) {

            goalsLabel.setText( "");
            goalsLabel.setVisibility(View.VISIBLE);

        } else {
            goalsLabel.setText(goals.size() + "");
            goalsLabel.setVisibility(View.VISIBLE);
        }
        boolean hasSubst = false;
        for (Incident incident : event.getIncidents()) {
            if (incident.getSubtype().equals("subst") && incident.getParticipantId() == lineupItem.getPlayerId()) {
                hasSubst = true;
                break;
            }
        }

        /*FluentIterable.from(event.getIncidents()).anyMatch(new Predicate<Incident>() {
            @Override
            public boolean apply(Incident input) {
                return input.getSubtype().equals("subst") && input.getParticipantId() == lineupItem.getPlayerId();
            }
        });*/
        mView.findViewById(R.id.subst_incident_icon).setVisibility(hasSubst ? View.VISIBLE : View.GONE);
        Incident cardIncident = null;
        for (Incident incident : event.getIncidents()) {
            if (incident.getSubtype().equals("card") &&
                    incident.getParticipantId() == lineupItem.getPlayerId()) cardIncident = incident;
        }

        /*FluentIterable.from(event.getIncidents()).filter(new Predicate<Incident>() {
            @Override
            public boolean apply(Incident input) {
                return input.getSubtype().equals("card") && input.getParticipantId() == lineupItem.getPlayerId();
            }
        }).last().orNull();*/
        if (cardIncident != null) {
            mView.findViewById(R.id.card_incidents).setVisibility(View.VISIBLE);
            ImageResourceHelper.getInstance(mView.getContext()).bind(cardIncident, (ImageView) mView.findViewById(R.id.subst_incident_icon));
        } else {
            mView.findViewById(R.id.card_incidents).setVisibility(View.GONE);
        }
    }
}