package dk.releaze.android.sportcenter.views;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.views.PopupWindows;

/**
 * QuickAction dialog, shows action list as icon and text like the one in Gallery3D app. Currently supports vertical
 * and horizontal layout.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 *         <p/>
 *         Contributors:
 *         - Kevin Peck <kevinwpeck@gmail.com>
 */
public class QuickActionGrid extends PopupWindows {
    private View mRootView;
    private LayoutInflater mInflater;
    private GridView grid;

    private boolean mDidAction;

    private int mAnimStyle;
    private int rootWidth = 0;

    public static final int ANIM_GROW_FROM_LEFT = 1;
    public static final int ANIM_GROW_FROM_RIGHT = 2;
    public static final int ANIM_GROW_FROM_CENTER = 3;
    public static final int ANIM_REFLECT = 4;
    public static final int ANIM_AUTO = 5;
    public static final int ANIM_TRANSLATE_FROM_BOTTOM = 6;

    public QuickActionGrid(Context context) {
        super(context);

        mInflater = LayoutInflater.from(context);

        setRootViewId(R.layout.dk_releaze_view_popup_grid);

        mAnimStyle = ANIM_TRANSLATE_FROM_BOTTOM;
    }

    /**
     * Set root view.
     *
     * @param id
     *         Layout resource id
     */
    public void setRootViewId(int id) {
        mRootView = (ViewGroup) mInflater.inflate(id, null);
        grid = (GridView) mRootView.findViewById(R.id.grid);

        mRootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        setContentView(mRootView);
    }

    public void setRootWidth(int rootWidth) {
        this.rootWidth = rootWidth;
        mWindow.setWidth(this.rootWidth);
        mRootView.setLayoutParams(new LayoutParams(this.rootWidth, LayoutParams.WRAP_CONTENT));
    }

    /**
     * Set animation style
     *
     * @param mAnimStyle
     *         animation style, default is set to ANIM_AUTO
     */
    public void setAnimStyle(int mAnimStyle) {
        this.mAnimStyle = mAnimStyle;
    }

    public void setAdapter(BaseAdapter adapter) {
        grid.setAdapter(adapter);
    }

    public void setOnItemClickListener(GridView.OnItemClickListener listener) {
        grid.setOnItemClickListener(listener);
    }

    /**
     * Show quickaction popup. Popup is automatically positioned, on top or bottom of anchor view.
     */
    public void show(View anchor) {
        preShow();

        int xPos, yPos;

        mDidAction = false;

        int[] location = new int[2];

        anchor.getLocationOnScreen(location);

        Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] + anchor.getHeight());

        mRootView.measure(rootWidth == 0 ? LayoutParams.MATCH_PARENT : rootWidth, LayoutParams.WRAP_CONTENT);
        int rootHeight = mRootView.getMeasuredHeight();

        if (rootWidth == 0) {
            rootWidth = mRootView.getMeasuredWidth();
        }

        int screenWidth = mWindowManager.getDefaultDisplay().getWidth();
        int screenHeight = mWindowManager.getDefaultDisplay().getHeight();

        //automatically get X coord of popup (top left)
        if ((anchorRect.left + rootWidth) > screenWidth) {
            xPos = anchorRect.left - (rootWidth - anchor.getWidth());
            xPos = (xPos < 0) ? 0 : xPos;
        } else {
            if (anchor.getWidth() > rootWidth) {
                xPos = anchorRect.centerX() - (rootWidth / 2);
            } else {
                xPos = anchorRect.left;
            }
        }

        int dyTop = anchorRect.top;
        int dyBottom = screenHeight - anchorRect.bottom;

        boolean onTop = (dyTop > dyBottom) ? true : false;

        if (onTop) {
            if (rootHeight > dyTop) {
                yPos = 15;
                LayoutParams l = grid.getLayoutParams();
                l.height = dyTop - anchor.getHeight();
            } else {
                yPos = anchorRect.top - rootHeight;
            }
        } else {
            yPos = anchorRect.bottom;

            if (rootHeight > dyBottom) {
                LayoutParams l = grid.getLayoutParams();
                l.height = dyBottom;
            }
        }

        setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);

        mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);
    }

    public void setNumColumns(int numColumns) {
        if (grid != null) grid.setNumColumns(numColumns);
    }

    /**
     * Set animation style
     *
     * @param screenWidth
     *         screen width
     * @param requestedX
     *         distance from left edge
     * @param onTop
     *         flag to indicate where the popup should be displayed. Set TRUE if displayed on top of anchor view
     *         and vice versa
     */
    private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
        switch (mAnimStyle) {
            case ANIM_GROW_FROM_LEFT:
                mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Left : R.style.Animations_PopDownMenu_Left);
                break;

            case ANIM_GROW_FROM_RIGHT:
                mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Right : R.style.Animations_PopDownMenu_Right);
                break;

            case ANIM_GROW_FROM_CENTER:
                mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Center : R.style.Animations_PopDownMenu_Center);
                break;

            case ANIM_REFLECT:
                mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Reflect : R.style.Animations_PopDownMenu_Reflect);
                break;

            case ANIM_TRANSLATE_FROM_BOTTOM:
                mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_FromBottom : R.style.Animations_PopDownMenu_Reflect);
                break;
        }
    }
}