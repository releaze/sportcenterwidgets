package dk.releaze.android.sportcenter.io;

/**
 * Created by gpl on 24.10.14.
 */
public class RetrofitException extends Exception {
    public RetrofitException() {
        super();
    }

    public RetrofitException(String detailMessage) {
        super(detailMessage);
    }

    public RetrofitException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public RetrofitException(Throwable throwable) {
        super(throwable);
    }
}
