package dk.releaze.android.sportcenter.fragments.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.releaze.android.sportcenter.io.Api;
import dk.releaze.android.sportcenter.io.RetrofitException;
import dk.releaze.android.sportcenter.model.Event;
import dk.releaze.android.sportcenter.model.Incident;
import dk.releaze.android.sportcenter.model.LineUpItem;
import dk.releaze.android.sportcenter.utils.ApiInitializer;


/**
 * Created by gpl on 24.10.14.
 */
public class LineUpLoader extends AsyncTaskLoader<LineUpLoader.Result> {

    private static final String LOG_TAG = LineUpLoader.class.getSimpleName();
    private final int mEventId;
    private final Api mApi;

    public LineUpLoader(Context context, int eventId) {
        super(context);
        mEventId = eventId;
        mApi = ApiInitializer.getInstance(context).get();
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public Result loadInBackground() {
        try {
            Event event = mApi.event(mEventId);
            List<LineUpItem> home = mApi.lineup(mEventId, event.getHomeId());
            Map<Integer, LineUpItem> homeMap = toMap(home);
//            Map<Integer, LineUpItem> homeMap = Maps.uniqueIndex(home, new Function<LineUpItem, Integer>() {
//                @Override
//                public Integer apply(LineUpItem input) {
//                    return input.getPlayerId();
//                }
//            });
            for (Incident incident : event.getIncidents()) {
                if ("Substitution in".equalsIgnoreCase(incident.getType())) {
                    if (homeMap.containsKey(incident.getParticipantId())) {
                        homeMap.get(incident.getParticipantId()).setSubsType(LineUpItem.SubsType.SUB_IN);
                    }
                } else if ("Substitution out".equalsIgnoreCase(incident.getType())) {
                    if (homeMap.containsKey(incident.getParticipantId())) {
                        homeMap.get(incident.getParticipantId()).setSubsType(LineUpItem.SubsType.SUB_OUT);
                    }
                }
            }

            List<LineUpItem> away = mApi.lineup(mEventId, event.getAwayId());
            Map<Integer, LineUpItem> awayMap = toMap(away);
//            Map<Integer, LineUpItem> awayMap = Maps.uniqueIndex(away, new Function<LineUpItem, Integer>() {
//                @Override
//                public Integer apply(LineUpItem input) {
//                    return input.getPlayerId();
//                }
//            });
            for (Incident incident : event.getIncidents()) {
                if ("Substitution in".equalsIgnoreCase(incident.getType())) {
                    if (awayMap.containsKey(incident.getParticipantId())) {
                        awayMap.get(incident.getParticipantId()).setSubsType(LineUpItem.SubsType.SUB_IN);
                    }
                } else if ("Substitution out".equalsIgnoreCase(incident.getType())) {
                    if (awayMap.containsKey(incident.getParticipantId())) {
                        awayMap.get(incident.getParticipantId()).setSubsType(LineUpItem.SubsType.SUB_OUT);
                    }
                }
            }

            return new Result(home, away, event);
        } catch (RetrofitException error) {
            Log.e(LOG_TAG, "No lineups for this match");
            return null;
        }
    }

    private static Map<Integer, LineUpItem> toMap(List<LineUpItem> list) {
        Map<Integer, LineUpItem> result = new HashMap<Integer, LineUpItem>();
        for (LineUpItem item : list) {
            result.put(item.getPlayerId(), item);
        }
        return result;
    }

    public static class Result {
        private final List<LineUpItem> mHome;
        private final List<LineUpItem> mAway;
        private final Event mEvent;

        public Result(List<LineUpItem> home, List<LineUpItem> away, Event event) {
            mHome = home;
            mAway = away;
            mEvent = event;
        }

        public List<LineUpItem> getHomeTeam() {
            return mHome;
        }

        public List<LineUpItem> getAwayTeam() {
            return mAway;
        }

        public Event getEvent() {
            return mEvent;
        }
    }
}
