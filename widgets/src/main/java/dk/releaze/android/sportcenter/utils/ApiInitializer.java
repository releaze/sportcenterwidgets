package dk.releaze.android.sportcenter.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import dk.releaze.android.sportcenter.io.Api;

import dk.releaze.android.sportcenter.io.RetrofitException;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by gpl on 24.10.14.
 */
public class ApiInitializer {
    private static final String REST_API_URL = "http://ss2.tjekscores.dk";
    private static final String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static ApiInitializer sInstance;

    public static ApiInitializer getInstance(Context context) {
        if (sInstance == null) sInstance = new ApiInitializer(context);
        return sInstance;
    }

    private final Api mApi;

    private ApiInitializer(final Context context) {
        mApi = new RestAdapter.Builder()
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("x-ss-auth", "basic auth");
                        request.addHeader("x-ss-application", "app name");
                        request.addHeader("x-ss-applicationVersion", "app version");
                        request.addHeader("Accept-Language", Locale.getDefault().toString());
                        request.addHeader("Application-Name", context.getPackageName());
                    }
                })
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        if (cause != null && cause.isNetworkError()) {
                            Response r = cause.getResponse();
                            if (r != null) {
                                Log.e("RETROFIT_ERROR", r.getReason() + " " + r.getUrl());
                            }
                            Log.e("RETROFIT_ERROR", "unknown reason");
                        }
                        return new RetrofitException(cause);
                    }
                })
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setEndpoint(REST_API_URL).setConverter(new GsonConverter(new GsonBuilder()
                        .setDateFormat(API_DATE_FORMAT)
                        .registerTypeAdapter(Date.class, new ISODateAdapter())
                        .registerTypeAdapter(Boolean.class, new BooleanTypeAdapter())
                        .registerTypeAdapter(boolean.class, new BooleanTypeAdapter())
                        .create()))
                .build()
                .create(Api.class);
    }

    public Api get() {
        return mApi;
    }

    private static class ISODateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
        private static final DateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
        private static final DateFormat ISO_8601_DATE_FORMAT = new SimpleDateFormat(ApiInitializer.API_DATE_FORMAT);

        static {
            ISO_8601_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
            String dateFormatAsString = ISO_8601_DATE_FORMAT.format(src);
            return new JsonPrimitive(dateFormatAsString);
        }

        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            if (!(json instanceof JsonPrimitive)) {
                throw new JsonParseException("The date should be a string value");
            }
            Date date = deserializeToDate(json);
            if (typeOfT == Date.class) {
                return date;
            } else {
                throw new IllegalArgumentException(getClass() + " cannot deserialize to " + typeOfT);
            }
        }

        private Date deserializeToDate(JsonElement json) {
            // TODO refactor deserialization
            try {

                return ISO_8601_DATE_FORMAT.parse(json.getAsString());
            } catch (ParseException e) {
                try {
                    return SIMPLE_DATE_FORMAT.parse(json.getAsString());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
            return null;
        }
    }

    private static class BooleanTypeAdapter extends TypeAdapter<Boolean> {
        @Override
        public void write(JsonWriter out, Boolean value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }

        @Override
        public Boolean read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case BOOLEAN:
                    return in.nextBoolean();
                case NULL:
                    return false;
                case NUMBER:
                    return in.nextInt() != 0;
                case STRING:
                    return Boolean.parseBoolean(in.nextString());
                default:
                    throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
            }
        }
    }
}
