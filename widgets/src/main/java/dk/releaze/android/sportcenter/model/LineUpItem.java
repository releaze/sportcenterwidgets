package dk.releaze.android.sportcenter.model;

/**
 * Created by gpl on 24.10.14.
 */
public class LineUpItem {
    private String name;
    private int number;
    private int positionCode;
    private int countryId;
    private String country;
    private int positionEnetType;
    private String positionName;
    private int playerId;
    private boolean isNew;
    private boolean coach;

    private SubsType subsType = SubsType.NONE;

    private float x, y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public int getPositionCode() {
        return positionCode;
    }

    public int getCountryId() {
        return countryId;
    }

    public String getCountry() {
        return country;
    }

    public int getPositionEnetType() {
        return positionEnetType;
    }

    public String getPositionName() {
        return positionName;
    }

    public int getPlayerId() {
        return playerId;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public String getShortName() {

        String[] chunks = name.split("[ -]");
        String newName = name;
        if (chunks.length > 1) {
            for (int i = 0; i < chunks.length - 1; i++) {
                if (chunks[i].length() > 0) {
                    newName = newName.replace(chunks[i], chunks[i].charAt(0) + ".");
                }
            }
        }
        return newName;
    }

    int mPositionX = -1;
    int mPositionY = -1;

    public boolean isSubs() {
        return positionEnetType == 5;
    }

    public boolean isInj() {
        return positionEnetType == 7;
    }

    public boolean isSusp() {
        return positionEnetType == 8;
    }

    public boolean isCoach() {
        return positionEnetType == 10;
    }

    public boolean isBench() {
        return isInj() || isSubs() || isSusp();
    }


    private void parseXYPosition() {
        String position = String.valueOf(getPositionCode());
        if (position.length() > 2) {
            mPositionY = Integer.parseInt(position.substring(0, 2));
            mPositionX = Integer.parseInt(position.substring(2, 3));
        } else {
            mPositionY = Integer.parseInt(position.substring(0, 1));
            mPositionX = Integer.parseInt(position.substring(1, 2));
        }
    }


    public int getPositionX() {
        if (mPositionX == -1) parseXYPosition();
        return mPositionX;
    }

    public int getPositionY() {
        if (mPositionY == -1) parseXYPosition();
        return mPositionY;
    }

    public SubsType getSubsType() {
        return subsType;
    }

    public void setSubsType(SubsType subsType) {
        this.subsType = subsType == null ? SubsType.NONE : subsType;
    }

    public static enum SubsType {
        NONE,
        SUB_IN,
        SUB_OUT
    }
}