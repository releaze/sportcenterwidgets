package dk.releaze.android.sportcenter.fragments.controllers;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.model.vo.LeagueTableItem;
import dk.releaze.android.sportcenter.utils.ResourceHelper;
import dk.releaze.android.sportcenter.views.CircledTextView;


/**
 * Created by gpl on 24.10.14.
 */
public class TournamentTableListItemViewController {
    private Context mContext;
    private int mDefaultNumberColor;
    private CircledTextView mNumber;
    private TextView mTeam;
    private TextView mMatches;
    private TextView mGoals;
    private TextView mPoints;

    public void setView(View view) {
        mContext = view.getContext();
        mNumber = (CircledTextView) view.findViewById(R.id.number);
        mTeam = (TextView) view.findViewById(R.id.team);
        mMatches = (TextView) view.findViewById(R.id.matches);
        mGoals = (TextView) view.findViewById(R.id.goals);
        mPoints = (TextView) view.findViewById(R.id.points);
        mDefaultNumberColor = mContext.getResources().getColor(R.color.dk_releaze_text_main_block_text);
    }

    public void update(int id, LeagueTableItem item, boolean allowCircleDrawing) {
        if (item == null) return;
        if (allowCircleDrawing) {
            if (item.getRankType() != null) {
                int resId = ResourceHelper.getColorId(item.getRankType(), mContext);
                if (!ResourceHelper.isNotFound(resId)) {
                    mNumber.drawCircle(mContext.getResources().getColor(resId));
                    mNumber.setTextColor(Color.WHITE);
                } else {
                    // if rankType color is undefined it is better to undraw circle
                    undrawCircle();
                }
            }
        } else {
            undrawCircle();
        }
        mNumber.setText(String.valueOf(id));
        mTeam.setText(item.getTeamName());
        mMatches.setText(String.valueOf(item.getMatchesPlayed()));
        mGoals.setText(item.getGoalsScored() + "/" + item.getGoalsConceded());
        mPoints.setText(String.valueOf(item.getPoints()));
    }

    private void undrawCircle() {
        mNumber.unDrawCircle();
        mNumber.setTextColor(mDefaultNumberColor);
    }
}
