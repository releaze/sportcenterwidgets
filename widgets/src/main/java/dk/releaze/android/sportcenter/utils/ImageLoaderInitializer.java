package dk.releaze.android.sportcenter.utils;

import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * Created by gpl on 24.10.14.
 */
public class ImageLoaderInitializer {
    private static ImageLoaderInitializer sInstance;

    public static ImageLoaderInitializer getInstance(Context context) {
        if (sInstance == null) sInstance = new ImageLoaderInitializer(context);
        return sInstance;
    }

    private ImageLoaderInitializer(Context context) {
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "rlz/tshirts");
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // 2 Mb
                .discCache(new UnlimitedDiscCache(cacheDir))
                .build();
        ImageLoader.getInstance().init(config);
    }

    public ImageLoader get() {
        return ImageLoader.getInstance();
    }
}
