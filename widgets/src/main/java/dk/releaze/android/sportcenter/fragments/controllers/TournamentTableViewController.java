package dk.releaze.android.sportcenter.fragments.controllers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import dk.releaze.android.sportcenter.R;
import dk.releaze.android.sportcenter.model.vo.LeagueTableItem;
import dk.releaze.android.sportcenter.model.vo.TypedLeagueTableItems;
import dk.releaze.android.sportcenter.views.ToggleButtonSelector;


/**
 * Created by gpl on 24.10.14.
 */
public class TournamentTableViewController {
    private TextView mTitle;
    private ToggleButton mTable;
    private ToggleButton mHome;
    private ToggleButton mAway;
    private InnerAdapter mAdapter = new InnerAdapter();
    private ToggleButtonSelector mSelector = new ToggleButtonSelector();

    public void setView(View view) {
        mTitle = (TextView) view.findViewById(R.id.title);
        mTable = (ToggleButton) view.findViewById(R.id.button_table);
        mHome = (ToggleButton) view.findViewById(R.id.button_home);
        mAway = (ToggleButton) view.findViewById(R.id.button_away);
        mSelector.add(mTable);
        mSelector.add(mHome);
        mSelector.add(mAway);
        ListView listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(mAdapter);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void update(final TypedLeagueTableItems items) {
        if (items == null) return;
        View.OnClickListener tableClickListener = new InnerListener(mTable, items.getReal());
        mTable.setOnClickListener(tableClickListener);
        mHome.setOnClickListener(new InnerListener(mHome, items.getHome()));
        mAway.setOnClickListener(new InnerListener(mAway, items.getAway()));
        tableClickListener.onClick(mTable);
    }

    private class InnerAdapter extends BaseAdapter /*implements View.OnClickListener*/ {
        private List<LeagueTableItem> mItems = new ArrayList<LeagueTableItem>();
        private boolean mIsCircleDrawingAllowed;

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int i) {
            return mItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return mItems.get(i).hashCode();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.dk_releaze_view_league_table_list_item,
                                viewGroup,
                                false);
            }
            int id = i + 1;
            LeagueTableItem item = mItems.get(i);
            TournamentTableListItemViewController controller = new TournamentTableListItemViewController();
            controller.setView(view);
            controller.update(id, item, mIsCircleDrawingAllowed);
            view.setTag(item.getTeamId());
            return view;
        }

        void update(List<LeagueTableItem> items, boolean allowCircleDrawing) {
            mIsCircleDrawingAllowed = allowCircleDrawing;
            mItems.clear();
            if (items != null) mItems.addAll(items);
            notifyDataSetChanged();
        }
    }

    private class InnerListener implements View.OnClickListener {
        private final List<LeagueTableItem> mItems;
        private final ToggleButton mButton;

        InnerListener(ToggleButton button, List<LeagueTableItem> items) {
            mButton = button;
            mItems = items;
        }

        @Override
        public void onClick(View view) {
            mSelector.select(mButton);
            mAdapter.update(mItems, mButton.equals(mTable));
        }
    }
}
